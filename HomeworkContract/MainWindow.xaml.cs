﻿using System.IO;
using System.Windows;
using System.Windows.Xps;
using System.Printing;
using System.Windows.Documents;
using System;

namespace HomeworkContract
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            
        }

        private void ButtonClick(object sender, RoutedEventArgs e)
        {
            PrintDocument(flowDocument);
        }

        private void PrintDocument(FlowDocument flowDocument)
        {
            MemoryStream memoryStream = new MemoryStream();
            TextRange textRange = new TextRange(flowDocument.ContentStart, flowDocument.ContentEnd);
            textRange.Save(memoryStream, DataFormats.Xaml);
            //FlowDocument flowDocument = new FlowDocument();
            //TextRange textRange = new TextRange(flowDocument.ContentStart, flowDocument.ContentEnd);
            textRange.Load(memoryStream, DataFormats.Xaml);

            PrintDocumentImageableArea printDocumentImageableArea = null;
            XpsDocumentWriter xpsDocumentWriter = PrintQueue.CreateXpsDocumentWriter(ref printDocumentImageableArea);
            if(xpsDocumentWriter !=null && printDocumentImageableArea!=null)
            {
                DocumentPaginator documentPaginator = ((IDocumentPaginatorSource)flowDocument).DocumentPaginator;
                documentPaginator.PageSize = new Size(printDocumentImageableArea.MediaSizeWidth, printDocumentImageableArea.MediaSizeHeight);
                Thickness thickness = new Thickness(72);
                flowDocument.PagePadding = new Thickness
                    (
                    Math.Max(printDocumentImageableArea.OriginWidth, thickness.Left),
                    Math.Max(printDocumentImageableArea.OriginHeight, thickness.Top),
                    Math.Max(printDocumentImageableArea.MediaSizeWidth - (printDocumentImageableArea.OriginWidth + printDocumentImageableArea.ExtentWidth), thickness.Right),
                    Math.Max(printDocumentImageableArea.MediaSizeHeight - (printDocumentImageableArea.OriginHeight + printDocumentImageableArea.ExtentHeight), thickness.Bottom)
                );

                flowDocument.ColumnWidth = double.PositiveInfinity;
                xpsDocumentWriter.Write(documentPaginator);
            }
        }
    }
}
